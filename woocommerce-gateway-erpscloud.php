<?php
/**
 * Plugin Name: WooCommerce ErpsCloud Gateway
 * Plugin URI: https://wordpress.org/plugins/woocommerce-gateway-erpscloud/
 * Description: Take payments on your store using ErpsCloud.
 * Author: Vanand Mkrtchyan
 * Author URI: http://www.vm-development.com/
 * Version: 1.0.0
 * Requires at least: 4.4
 * WC requires at least: 2.6
 * Text Domain: woocommerce-gateway-erpscloud
 * Domain Path: /languages
 *
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * WooCommerce fallback notice.
 *
 * @since 4.1.2
 * @return string
 */
function woocommerce_erpscloud_missing_wc_notice()
{
    /* translators: 1. URL link. */
    echo '<div class="error"><p><strong>' . sprintf(esc_html__('ErpsCloud requires WooCommerce to be installed and active. You can download %s here.', 'woocommerce-gateway-erpscloud'), '<a href="https://woocommerce.com/" target="_blank">WooCommerce</a>') . '</strong></p></div>';
}

add_action('plugins_loaded', 'woocommerce_gateway_erpscloud_init');

function woocommerce_gateway_erpscloud_init()
{
    load_plugin_textdomain('woocommerce-gateway-erpscloud', false, plugin_basename(dirname(__FILE__)) . '/languages');

    if (!class_exists('WooCommerce')) {
        add_action('admin_notices', 'woocommerce_erpscloud_missing_wc_notice');
        return;
    }

    if (!class_exists('WC_Erpscloud')) :

        define('WC_ERPSCLOUD_VERSION', '1.0.0');
        define('WC_ERPSCLOUD_MIN_PHP_VER', '5.6.0');
        define('WC_ERPSCLOUD_MIN_WC_VER', '2.6.0');
        define('WC_ERPSCLOUD_MAIN_FILE', __FILE__);
        define('WC_ERPSCLOUD_PLUGIN_URL', untrailingslashit(plugins_url(basename(plugin_dir_path(__FILE__)), basename(__FILE__))));
        define('WC_ERPSCLOUD_PLUGIN_PATH', untrailingslashit(plugin_dir_path(__FILE__)));

        class WC_Erpscloud
        {

            /**
             * @var Singleton The reference the *Singleton* instance of this class
             */
            private static $instance;

            /**
             * Returns the *Singleton* instance of this class.
             *
             * @return Singleton The *Singleton* instance.
             */
            public static function get_instance()
            {
                if (null === self::$instance) {
                    self::$instance = new self();
                }
                return self::$instance;
            }

            /**
             * Private clone method to prevent cloning of the instance of the
             * *Singleton* instance.
             *
             * @return void
             */
            private function __clone()
            {
            }

            /**
             * Private unserialize method to prevent unserializing of the *Singleton*
             * instance.
             *
             * @return void
             */
            private function __wakeup()
            {
            }

            /**
             * Protected constructor to prevent creating a new instance of the
             * *Singleton* via the `new` operator from outside of this class.
             */
            private function __construct()
            {
                add_action('admin_init', array($this, 'install'));
                $this->init();
            }


            /**
             * Init the plugin after plugins_loaded so environment variables are set.
             *
             * @since 1.0.0
             * @version 4.0.0
             */
            public function init()
            {
                require_once(dirname(__FILE__) . '/includes/class-wc-gateway-erpscloud.php');
                //
                add_filter('woocommerce_payment_gateways', array($this, 'add_gateways'));
                add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'plugin_action_links'));
            }


            /**
             * Adds plugin action links.
             *
             * @since 1.0.0
             * @version 4.0.0
             */
            public function plugin_action_links($links)
            {
                $plugin_links = array(
                    '<a href="admin.php?page=wc-settings&tab=checkout&section=erpscloud">' . esc_html__('Settings', 'woocommerce-gateway-erpscloud') . '</a>',
                    '<a href="https://dashboard.erpscloud.com/documentation">' . esc_html__('Docs', 'woocommerce-gateway-erpscloud') . '</a>'
                );
                return array_merge($plugin_links, $links);
            }


            /**
             * Add the gateways to WooCommerce.
             *
             * @since 1.0.0
             * @version 4.0.0
             */
            public function add_gateways($methods)
            {
                $methods[] = 'WC_Gateway_ErpsCloud';

                return $methods;
            }


            /**
             * Handles upgrade routines.
             *
             * @since 3.1.0
             * @version 3.1.0
             */
            public function install()
            {
                if (!is_plugin_active(plugin_basename(__FILE__))) {
                    return;
                }

                if (!defined('IFRAME_REQUEST') && (WC_ERPSCLOUD_VERSION !== get_option('wc_erpscloud_version'))) {
                    do_action('woocommerce_erpscloud_updated');

                    if (!defined('WC_ERPSCLOUD_INSTALLING')) {
                        define('WC_ERPSCLOUD_INSTALLING', true);
                    }

                    $this->update_plugin_version();
                }
            }

            /**
             * Updates the plugin version in db
             *
             * @since 3.1.0
             * @version 4.0.0
             */
            public function update_plugin_version()
            {
                delete_option('wc_erpscloud_version');
                update_option('wc_erpscloud_version', WC_ERPSCLOUD_VERSION);
            }
        }

        WC_Erpscloud::get_instance();

    endif;
}
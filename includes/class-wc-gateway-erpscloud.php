<?php
if (!defined('ABSPATH')) {
    exit;
}

/**
 * WC_Gateway_ErpsCloud class.
 *
 * @extends WC_Payment_Gateway
 */
class WC_Gateway_ErpsCloud extends WC_Payment_Gateway
{

    // TODO add ssl to url
    static public $base_url = 'http://sandbox.erpscloud.com/api/';

    /**
     * WC_Gateway_ErpsCloud constructor.
     */
    public function __construct()
    {
        $this->id = 'erpscloud';
        $this->method_title = __('Ophshore Payment', 'woocommerce-gateway-erpscloud');
        $this->method_description = sprintf(__('ErpsCloud works by adding payment fields on the checkout and then sending the details to ErpsCloud.'));
        $this->has_fields = true;


        // Get setting values.
        $this->title = $this->get_option('title');
        $this->icon = plugin_dir_url(dirname(__FILE__)) . 'assets/images/credit-cards.png';
        $this->mode = $this->get_option('mode');
        $this->description = $this->get_option('description');
        $this->enabled = $this->get_option('enabled');
        $this->api_token = $this->get_option('api_token');
        $this->api_secret = $this->get_option('api_secret');

        if ($this->mode == 'yes')
            self::$base_url = 'https://dashboard.erpscloud.com/api/';

        $this->init_form_fields();

        $this->init_settings();

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        add_action('woocommerce_thankyou', array($this, 'thankyou'), 10, 1);

    }

    /**
     * Process thank you page
     *
     * @param $order_id
     */
    public static function thankyou($order_id)
    {
        if (!$order_id)
            return;

        // Getting an instance of the order object
        $order = wc_get_order($order_id);

        if (!$order->is_paid()) {
            $order->update_status('wc-completed');
        }
    }

    /**
     * Initialise Gateway Settings Form Fields
     */
    public function init_form_fields()
    {
        $this->form_fields = require(dirname(__FILE__) . '/admin/erpscloud-settings.php');
    }

    /**
     * Processing the payment request
     *
     * @param int $order_id
     *
     * @return array
     * @throws Exception
     */
    public function process_payment($order_id)
    {
        $order = wc_get_order($order_id);

        try {
            $result = json_decode($this->sendPaymentRequest($order));

            if ($result->statusCode != 201) {
                wc_add_notice($result->content, 'error');
                return array(
                    'result' => 'failure',
                    'messages' => $result->content
                );
            }
            return array(
                'result' => 'success',
                'redirect' => $result->content
            );

        } catch (\Exception $e) {
            $order->update_status('failed');
            wc_add_notice($e->getMessage(), 'error');
            return array(
                'result' => 'failure',
                'messages' => $e->getMessage()
            );
        }
    }

    /**
     * Mutate WC fields with API fields
     *
     * @return array
     */
    public function mutateFields()
    {
        $server_fields = $this->getFields();
        $server_fields = $server_fields->content;

        $fields = array();
        $mapping = $this->getFieldsMapping();
        foreach ($server_fields as $field) {
            $fields[$field->name] = $_POST[$mapping[$field->name]];
            if (isset($_POST[$mapping[$field->name]]))
                $fields[$field->name] = $_POST[$mapping[$field->name]];
            else
                $fields[$field->name] = null;
        }

        return $fields;
    }

    /**
     * Get required fields list.
     *
     * @return array|mixed|object
     */
    public function getFields()
    {
        $endpoint = "order/create";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$base_url . $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'X-Access-Token: ' . $this->api_token
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return json_decode($server_output);
    }

    /**
     * Get array of keys to map.
     *
     * @return array
     */
    public function getFieldsMapping()
    {
        return array(
            'first_name' => 'billing_first_name',
            'last_name' => 'billing_last_name',
            'country' => 'billing_country',
            'state' => 'billing_state',
            'city' => 'billing_city',
            'address' => 'billing_address_1',
            'telephone' => 'billing_phone',
            'zip' => 'billing_postcode',
        );
    }

    /**
     * Calculate SHA256 Signature to validate request.
     *
     * @param array $fields
     *
     * @return false|string
     */
    public function calculateSignature(array $fields)
    {
        ksort($fields);
        return hash_hmac('sha256', http_build_query($fields), $this->api_secret);
    }

    /**
     * Send request with fields to ErpsCloud API.
     *
     * @param WC_Order $order
     *
     * @return mixed
     */
    public function sendPaymentRequest(WC_Order $order)
    {
        $fields = $this->mutateFields();
        $fields['detected_client_ip'] = $_SERVER['REMOTE_ADDR'];
        $fields['date'] = date('d/m/Y H:i:s');
        $fields['amount'] = $order->get_total();
        $fields['product_description'] = 'some description';
        $fields['signature'] = $this->calculateSignature($fields);

        $endpoint = 'order';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$base_url . $endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'X-Access-Token: ' . $this->api_token
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //Post Fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, array_merge($fields, [
            'success_url' => $order->get_checkout_order_received_url(),
            'cancel_url' => $order->get_cancel_order_url_raw('/')
        ]));

        $server_output = curl_exec($ch);

        curl_close($ch);

        return $server_output;
    }
}

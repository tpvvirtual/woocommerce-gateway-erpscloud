<?php
if (!defined('ABSPATH')) {
    exit;
}

return apply_filters('wc_erpscloud_settings',
    array(
        'enabled' => array(
            'title' => __('Enable/Disable', 'woocommerce-gateway-erpscloud'),
            'label' => __('Enable ErpsCloud', 'woocommerce-gateway-erpscloud'),
            'type' => 'checkbox',
            'description' => '',
            'default' => 'no',
        ),
        'mode' => array(
            'title' => __('Live/Sandbox', 'woocommerce-gateway-erpscloud'),
            'label' => __('ErpsCloud Mode', 'woocommerce-gateway-erpscloud'),
            'type' => 'checkbox',
            'description' => 'Checked means live',
            'default' => 'no',
        ),
        'title' => array(
            'title' => __('Title', 'woocommerce-gateway-erpscloud'),
            'type' => 'text',
            'description' => __('This controls the title which the user sees during checkout.', 'woocommerce-gateway-erpscloud'),
            'default' => __('Credit Card (ErpsCloud)', 'woocommerce-gateway-erpscloud'),
            'desc_tip' => true,
        ),
        'description' => array(
            'title' => __('Description', 'woocommerce-gateway-erpscloud'),
            'type' => 'text',
            'description' => __('This controls the description which the user sees during checkout.', 'woocommerce-gateway-erpscloud'),
            'default' => __('Pay with your credit card via ErpsCloud.', 'woocommerce-gateway-erpscloud'),
            'desc_tip' => true,
        ),
        'api_token' => array(
            'title' => __('API Token', 'woocommerce-gateway-erpscloud'),
            'type' => 'text',
            'description' => __('This is API Token.', 'woocommerce-gateway-erpscloud'),
            'default' => "",
            'desc_tip' => true,
        ),
        'api_secret' => array(
            'title' => __('API Secret', 'woocommerce-gateway-erpscloud'),
            'type' => 'text',
            'description' => __('This controls API Secret.', 'woocommerce-gateway-erpscloud'),
            'default' => "",
            'desc_tip' => true,
        ),
    )
);
